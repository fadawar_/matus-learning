
<div class="form-group">
    <div class="row">
        <div class="col-md-1">
            {{ Form::checkbox('answer['.$i.'][correct]', 1, $answer['correct'],
            ['title' => trans('question-groups.correct_ans')]) }}
        </div>
        <div class="col-md-10">
            {{ Form::text('answer['.$i.'][answer_text]',
                isset($answer['text']) ? $answer['text'] : null,
                array('class'=>'form-control', 'placeholder'=>trans('question-groups.answer'))) }}
        </div>
        <div class="col-md-1">
            <button type="button" class="btn btn-large btn-warning remove_answer"
                    title="{{ trans('question-groups.remove') }}">X</button>
        </div>
    </div>
</div>