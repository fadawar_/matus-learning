<?php
/**
 * Created by PhpStorm.
 * User: fadawar
 * Date: 18.9.2014
 * Time: 10:57
 */

class TestSolution extends Eloquent {

    protected $fillable = array('user_id', 'max_points');

    public function user()
    {
        return $this->hasOne('User');
    }

    public function solutionQuestions()
    {
        return $this->hasMany('SolutionQuestion');
    }
} 