Matúš learning
==============

Systém na učenie a testovanie hasičov na Technickej univerzite vo Zvolene

**Použité technologie:**
Laravel 4.2.8,
Bootstrap 3.2,
PostgreSQL

Toto sú cesty spolu s popisom funkcionality
-------------------------------------------
* _question-groups_
    - ukaze zoznam skupin otazok
* _question-groups/1_
    - ukaze zoznam otazok v danej skupine
* _question-groups/new_
    - Formular na vytvorenie novej skupiny otazok - nazov skupiny. Kolkobodove otazky su v tejto skupine.
      Taktiez formular na vytvorenie novej otazky. Malo by sa to robit cez AJAX.
        + Na vrchu meno skupiny otazok
        + pod tym uz pridane otazky
        + pod tym formular na novu otazku

    - Typy otazok:
        + a b c d                       - automaticka oprava
        + abcd viac spravnych odpovedi  - body iba sa všetky spravne
        + jednoslovna odpoved           - automaticka oprava, plus vediet aj odpoved bez diakritiky - nie!
        + dlhy text - nie!

* _tests_
    - zoznam testov
* _tests/1_
    - riesenie testu 1
* _tests/1/edit_
    - editovanie testov, hlavne odkedy do kedy su povolene vyplnovat
* _tests/new_
    - vytvorenie testu. Vybera sa meno, cas za kolko ho je treba vyplnit.
      Spustat a uzatvarat sa bude manualne.
      Potom tam bude formular takyto: **5** otazok zo skupiny **zakladanie poziara**


* _answers_
    - zoznam poslednych 30 odpovedi na testy
* _answers/1_
    - konkretne vyplneny test. Vypise otazky aj s odpovedami. Filter na zobrazit neodohnotene otazky.
      Mozno zobrazit strukturu, aby sa dalo rychlo skakat na skupinu otazok.
      Dodatocne ohodnotenie - napisat pocet alebo cele zle, cele dobre.
      Taktiez tlacitko na ulozenie hodnotenia, aby na tom mohli pracovat viacery ludia.
      Ukladat, ktory pouzivatel to ohodnotil.

* _users/1_
    - profil pouzivatela, staci meno. Registrovat ineho pouzivatela by mohol iba niekto, kto uz je zaregistrovany.


Po vyplneni testu ho to presmeruje TestSolution. To má user_id, timestamps, a vlastne id. TestAnswers
su takto: id, test_solution_id, question_text, answer_text, correct. Ked sa user prihlasi, zobrazi sa mu dashboard.
Tam ma svoje test solutions a tlacitko poziadat o dalsi. Alebo ak este nerobil test, tak tlacidlo spustit test.


### Matúšove požiadavky
* Registrácia na boku
* na stránke dokumenty v odstavcoch (obsah)
* podpora videí
* po registrácii mozem pisat testy, po dokonceni sa to zorazi userovi a pošle niekomu na mail
* Len jeden jediný test
* admin môže dať znovu možnosť opakovať test, používateľ musí ale poslať žiadosť o opakovaní
* ked user spravi test, moze si pozriet čo spravil dobre a čo zle


### Moje otázky
* Stačí na text otázky a odpovede 255 znakov?

### TODO
* Pridat upravovanie otazky