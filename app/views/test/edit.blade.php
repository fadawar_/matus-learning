                <div class="row">

                    {{ Form::open(array('route'=>'test.update', 'method' => 'put',
                                        'class'=>'col-md-6 col-md-offset-3 form-horizontal test-edit')) }}
                    <h2>{{ $title }}</h2>

                    @if ($errors->has())
                        @include('layouts._errors')
                    @endif

                    @forelse ($question_groups as $q_group)
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <input type="number" name="questions_count[{{ $q_group->id }}]"
                                           min="0" max="{{ $q_group->questions_count }}"
                                           value="{{ $q_group->questions_in_test }}" class="form-control">
                                </div>
                                <div class="col-md-10 control-label">
                                    {{ trans('test.questions_from') }}
                                    {{ HTML::linkRoute('question-groups.show', $q_group->name, ['id' => $q_group->id]) }}
                                    ({{ $q_group->questions_count }}&NonBreakingSpace;{{ Lang::choice('test.count_of_questions', $q_group->questions_count) }})
                                </div>
                            </div>
                        </div>
                    @empty
                        <h3>{{ trans('question-groups.no_groups') }}</h3>
                    @endforelse

                    <div class="form-group">
                        {{ Form::submit(trans('test.save_btn'), array('class'=>'btn btn-large btn-success')) }}
                    </div>

                    {{ Form::close() }}
                </div>