<?php

return array(
    /* Destroy */
    'destroy_succ'      => 'Question was removed successfully.',

    /* Show page */
    'show_heading'      => 'Question',
    'delete_btn'        => 'Delete',

    /* Edit page */
    'edit_heading'      => 'Edit question',
    'update_btn'        => 'Update',
    'question_updated'  => 'Question was updated.',
);
