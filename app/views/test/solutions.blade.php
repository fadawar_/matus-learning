<div class="row">
    <h3>{{ trans('test.solutions_title') }}</h3>


    <div class="solutions-wrapper">
        @foreach ($solutions as $solution)
            <div class="solution">
                <h4 class="solution-title">
                    {{ $solution->created_at }}
                </h4>
                <ul>
                    <li>
                        {{ trans('test.achieved_points') }} : {{ $solution->achieved_points }}
                    </li>
                    <li>
                        {{ trans('test.max_points') }} : {{ $solution->max_points }}
                    </li>
                </ul>
            </div>
        @endforeach
    </div>
</div>