<?php

return array(
    /* Edit page */
    'edit_title'        => 'Edit test',
    'questions_from'    => 'questions from group',
    'save_btn'          => 'Save',
    'count_of_questions'=> '{0} questions|{1} question|[2,Inf] questions',

    /* Update */
    'success'           => 'Test was updated successfully.',
    'fail'              => 'The following errors occurred',

    /* Show page */
    'show_title'        => 'Test',
    'point_short'       => 'p',
    'send_btn'          => 'Send',

    /*** Test Solution ***/
    /* Store */
    'solution_id_not_found' => 'Your Test solution ID was not found in database.',
    'store_title'           => 'Test saved',
    'test_results'          => 'Congratulations! Test was saved. Your points: ',
    'solutions_title'       => 'My test solutions',
    'achieved_points'       => 'Achieved points',
    'max_points'            => 'Maximum points',
);