<?php

class TestSolutionsController extends BaseController {

    protected $layout = 'layouts.master';

    public function __construct()
    {
        $this->beforeFilter('csrf', array('on' => 'post'));
        $this->beforeFilter('auth');
    }

    public function show($id) {
        if ($id == Auth::id() || Auth::user()->isAdmin()) {
            $solutions = TestSolution::where('user_id', '=', $id)->get();

            $this->layout->solutions = $solutions;
            $this->layout->title = trans('test.solutions_title');
            $this->layout->content = 'test.solutions';
        }
        else {
            return Redirect::to('users/dashboard')
                ->with('msg_type', 'danger')
                ->with('message', trans('test.cannot_view_results'));
        }
    }

    public function store()
    {
        // TODO omg, toto treba cele refaktorovat
        $test_solution = TestSolution::find(Input::get('test_solution_id'));
        if ($test_solution) {
            $test_solution->achieved_points = 0;
            $generated_questions = $test_solution->solutionQuestions()->get();
            $answered_questions = Input::get('questions');

            foreach ($answered_questions as $question_id => $answer_ids) {
                // check answers
                $solution_question = $this->findSolutionQuestionByQuestionId($generated_questions, $question_id);
                if (isset($solution_question) && $solution_question->check_answers($answer_ids)) {
                    $real_question = Question::find($solution_question->question_id);
                    $points_for_question = QuestionGroup::find($real_question->question_group_id)->points;
                    $test_solution->achieved_points += $points_for_question;
                }

                // save answers
                $solution_answers = array();
                foreach ($answer_ids as $answer_id) {
                    $solution_answers[] = new SolutionAnswer(array(
                        'answer_id' => $answer_id,
                    ));
                }
                $solution_question->solutionAnswers()->saveMany($solution_answers);
            }
            // save test result
            $test_solution->save();
        } else {
            return Redirect::to('users/dashboard')
                ->with('msg_type', 'danger')
                ->with('message', trans('test.solution_id_not_found'));
        }

        // TODO spravit users.dashboard routes

        return Redirect::to('users/dashboard')
            ->with('msg_type', 'info')
            ->with('message', trans('test.test_results') . $test_solution->achieved_points);
    }

    public function findSolutionQuestionByQuestionId($solutionQuestions, $question_id)
    {
        foreach ($solutionQuestions as $solutionQuestion) {
            if ($solutionQuestion->question_id == $question_id) {
                return $solutionQuestion;
            }
        }
        return null;
    }
} 