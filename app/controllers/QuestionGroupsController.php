<?php

class QuestionGroupsController extends BaseController {

    protected $layout = 'layouts.master';

    public function __construct()
    {
        $this->beforeFilter('csrf', array('on' => 'post'));
        $this->beforeFilter('auth');
        $this->beforeFilter('auth.admin');
    }

    public function index()
    {
        $question_groups = QuestionGroup::orderBy('name')->paginate(10);
        $question_groups->getFactory()->setViewName('pagination::simple');
        $this->layout->title = trans('question-groups.index_heading');
        $this->layout->question_groups = $question_groups;
        $this->layout->content = 'question-groups.index';
    }

    public function create()
    {
        $this->layout->title = trans('question-groups.create_heading');
        $this->layout->content = 'question-groups.create';
    }

    public function store()
    {
        $validator = Validator::make(Input::all(), QuestionGroup::$rules);

        if ($validator->passes()) {
            $group = new QuestionGroup();
            $group->name = Input::get('name');
            $group->points = Input::get('points');
            $group->save();

            return Redirect::route('question-groups.show', [$group->id])
                ->with('msg_type', 'success')
                ->with('message', trans('question-groups.success'));
        } else {
            return Redirect::route('question-groups.create')
                ->with('msg_type', 'danger')
                ->with('message', trans('question-groups.fail'))
                ->withErrors($validator)
                ->withInput();
        }
    }

    public function show($id)
    {
        $group = QuestionGroup::findOrFail($id);
        $this->layout->question_group = $group;
        $this->layout->questions = $group->questions->all();
        $this->layout->title = trans('question-groups.show_heading');
        $this->layout->content = 'question-groups.show';
    }
} 