    <div class="alert alert-danger">
        @if(Session::has('message'))
        <h5><strong>{{ Session::get('message') }}:</strong></h5>
        @endif
        @foreach ($errors->all() as $error)
            {{ $error }}<br/>
        @endforeach
    </div>


