<!DOCTYPE html>
<html>
    @include('layouts.header')

    <body>
        @include('layouts.navigation')

        @if (Session::has('message') && count($errors) == 0)
            @include('layouts.flash')
        @endif

        <div class="container">

            @if (isset($content))
                @include($content)
            @endif

        </div>
    </body>
</html>