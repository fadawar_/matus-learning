<?php
/**
 * Created by PhpStorm.
 * User: fadawar
 * Date: 3.9.2014
 * Time: 14:08
 */

class Answer extends Eloquent {

    protected $fillable = array('correct', 'text');

    public function question()
    {
        return $this->belongsTo('Question');
    }
} 