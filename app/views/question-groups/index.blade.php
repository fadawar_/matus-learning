            <div class="row">
                <h2>{{ $title }}</h2>

                {{ HTML::linkRoute('question-groups.create', trans('question-groups.add_group_btn'), [], ['class' => 'btn btn-success']) }}

                @forelse ($question_groups as $q_group)
                    <h3>{{ HTML::linkRoute('question-groups.show', $q_group->name, ['id' => $q_group->id]) }}</h3>
                    <p><strong>{{ trans('question-groups.points') }}: {{ $q_group->points }}</strong>,
                               {{ trans('question-groups.created_at') }}: {{ $q_group->created_at }}</p>
                @empty
                    <h3>{{ trans('question-groups.no_groups') }}</h3>
                @endforelse
                {{ $question_groups->links() }}
            </div>