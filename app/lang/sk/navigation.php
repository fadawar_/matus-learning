<?php

return array(
    'app_name'          => 'Matúš learning',
    'toggle_navigation' => 'Zobraziť navigáciu',

    'register'          => 'Registrovať',
    'login'             => 'Prihlásiť',
    'logout'            => 'Odhlásiť',
    'question_groups'   => 'Skupiny otázok',

);
