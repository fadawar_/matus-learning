<div class="row">
    {{ Form::open(array('route'=>'test-solutions.store', 'class'=>'col-md-8 col-md-offset-2', 'id'=>'test-form')) }}
        {{ Form::hidden('test_solution_id', $test_solution_id) }}

        <h3>{{ trans('test.show_title') }}</h3>


        <div class="question-area">
            @foreach ($question_areas as $area)
                @foreach ($area as $question)
                    <div class="form-group">
                        <h4 class="question-text">
                            {{ $question->text }}
                        </h4>
                        @if ($question->correct_answers == 1)
                            @include('test._one_correct')
                        @else
                            @include('test._many_correct')
                        @endif
                    </div>
                @endforeach
            @endforeach
        </div>

        <div class="form-group">
            {{ Form::submit(trans('test.send_btn'), array(
                            'class'=>'btn btn-large btn-success btn-block')) }}
        </div>
    {{ Form::close() }}
</div>