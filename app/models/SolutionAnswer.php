<?php
/**
 * Created by PhpStorm.
 * User: fadawar
 * Date: 18.9.2014
 * Time: 11:03
 */

class SolutionAnswer extends Eloquent {

    protected $fillable = array('answer_id',);

    public function solutionQuestion()
    {
        return $this->belongsTo('SolutionQuestion');
    }

    public function answer()
    {
        return $this->hasOne('Answer');
    }
} 