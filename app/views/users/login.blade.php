            <div class="row">
                {{ Form::open(array('url'=>'users/signin', 'class'=>'col-md-4 col-md-offset-4')) }}
                    <h2 class="form-signin-heading">{{ trans('users.login_heading') }}</h2>

                    <div class="form-group">
                        {{ Form::text('email', null, array('class'=>'form-control', 'placeholder'=>trans('users.email_field'))) }}
                    </div>
                    <div class="form-group">
                        {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>trans('users.password_field'))) }}
                    </div>

                {{ Form::submit(trans('users.login_btn'), array('class'=>'btn btn-large btn-primary btn-block'))}}
                {{ Form::close() }}
            </div>