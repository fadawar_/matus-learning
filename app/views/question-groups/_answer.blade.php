
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-1">
                                {{ Form::checkbox('answer['.$i.'][correct]', 1,
                                isset($answer['correct']) ? true : false,
                                ['title' => trans('question-groups.correct_ans')]) }}
                            </div>
                            <div class="col-md-10">
                                {{ Form::text('answer['.$i.'][answer_text]',
                                    isset($answer['answer_text']) ? $answer['answer_text'] : null,
                                    array('class'=>'form-control', 'placeholder'=>trans('question-groups.answer'), 'type'=>'number')) }}
                            </div>
                            <div class="col-md-1">
                                <button type="button" class="btn btn-large btn-warning remove_answer"
                                        title="{{ trans('question-groups.remove') }}">X</button>
                            </div>
                        </div>
                    </div>