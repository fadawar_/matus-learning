<ul>
    <?php
        $answers = $question->answers->toArray();
        shuffle($answers)
    ?>
    @foreach ($answers as $answer)
        <li>{{ Form::radio('questions['.$question->id.'][]', $answer['id']) }} {{ $answer['text'] }}</li>
    @endforeach
</ul>