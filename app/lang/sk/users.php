<?php

return array(
    /* Login page */
    'login_heading'     => 'Prosím, prihláste sa',
    'email_field'       => 'Emailová adresa',
    'password_field'    => 'Heslo',
    'login_btn'         => 'Prihlásiť',

    /* Register page */
    'reg_heading'       => 'Prosím, registrujte sa',
    'firstname_field'   => 'Meno',
    'lastname_field'    => 'Priezvisko',
    'pass_conf_field'   => 'Potvrdiť heslo',
    'reg_btn'           => 'Registovať',
);
