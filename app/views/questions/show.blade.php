            <div class="row">
                <h2>{{ $title }}</h2>

                <div class="col-md-8 col-md-offset-2">
                    <h3>{{ $question->text }}</h3>

                    @foreach ($answers as $answer)
                    @if ($answer->correct)
                    <div class="answer correct-answer">
                        @else
                    <div class="answer">
                        @endif

                        {{ $answer->text }}
                    </div>
                    @endforeach



                    {{ Form::open(array('route' => array('question.destroy', $question->id), 'method' => 'delete')) }}
                        {{ HTML::linkRoute('question.edit', trans('question-groups.edit'), ['id' => $question->id],
                        ['class' => 'btn btn-primary']) }}

                        {{ Form::submit(trans('questions.delete_btn'), array('class'=>'btn btn-warning'))}}
                    {{ Form::close() }}

                </div>
            </div>

