<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolutionQuestionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('solution_questions', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('test_solution_id');
            $table->unsignedInteger('question_id');
            $table->timestamps();

            $table->foreign('test_solution_id')
                ->references('id')->on('test_solutions');
            $table->foreign('question_id')
                ->references('id')->on('questions');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('solution_questions');
	}

}
