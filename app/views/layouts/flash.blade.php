    <div class="container">
        <div class="alert alert-{{ Session::get('msg_type') }}">
            {{ Session::get('message') }}
        </div>
    </div>