<?php

return array(
    /* Create page */
    'create_heading'    => 'Create new question group',
    'name_field'        => 'Group name',
    'points_field'      => 'Points for a question',
    'save_btn'          => 'Save',

    /* Index page */
    'index_heading'     => 'Question groups',
    'points'            => 'Points',
    'created_at'        => 'created at',
    'no_groups'         => 'No question groups',
    'add_group_btn'     => 'Add new question group',

    /* Show page */
    'show_heading'      => 'Edit question group',
    'add_question'      => 'Add question',
    'random_sort'       => 'Answers will be sorted randomly.',
    'question?'         => 'Question?',
    'add_answer_btn'    => 'Add answer field',
    'correct_ans'       => 'Correct answer',
    'answer'            => 'Answer',
    'remove'            => 'Remove',
    'delete'            => 'Delete',
    'save_and_add_btn'  => 'Save question and add new',
    'err_occurred'      => 'The following errors occurred',
    'err_blank_answer'  => 'Answer cannot be blank.',
    'err_no_correct'    => 'At least one answer must be correct.',
    'question_saved'    => 'Question was saved successful!',
    'correct_answers'   => '{0} correct answers|{1} correct answer|[2,Inf] correct answers',
    'edit'              => 'Edit',

    /* Store */
    'success'           => 'Question group was saved successful!',
    'fail'              => 'The following errors occurred',
);
