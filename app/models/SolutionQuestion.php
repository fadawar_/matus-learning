<?php
/**
 * Created by PhpStorm.
 * User: fadawar
 * Date: 18.9.2014
 * Time: 11:01
 */

class SolutionQuestion extends Eloquent {

    protected $fillable = array('test_solution_id', 'question_id');

    public function testSolution()
    {
        return $this->belongsTo('TestSolution');
    }

    public function solutionAnswers()
    {
        return $this->hasMany('SolutionAnswer');
    }

    public function question()
    {
        return $this->belongsTo('Question');
    }

    public function check_answers($answer_ids)
    {
//        $correct_answers = $this->question()->answers()->where('correct', '=', true)->all();
        $correct_answers = Answer::where('question_id', '=', $this->question_id)->where('correct', '=', true)->get();
        if (sizeof($correct_answers) == sizeof($answer_ids)) {
            foreach ($correct_answers as $correct_answer) {
                if ( ! in_array($correct_answer->id, $answer_ids)) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }
} 