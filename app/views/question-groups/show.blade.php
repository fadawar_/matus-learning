
            <div class="row">
                <h2 class="form-signup-heading">{{{ $question_group->name }}}</h2>
                <p><strong>{{ trans('question-groups.points') }}: {{ $question_group->points }}</strong>,
                    {{ trans('question-groups.created_at') }}: {{ $question_group->created_at }}</p>

                <div class="col-md-8 col-md-offset-2">
                    <?php $i = 1; ?>
                    @foreach($question_group->questions as $question)
                    <div class="question">
                        <div class="row">
                            <div class="col-md-8">
                                <h4>{{ $i }}.
                                    {{ HTML::linkRoute('question.show', $question->text, ['id' => $question->id]) }}</h4>
                            </div>
                            <div class="col-md-2 col-md-offset-2">
                                {{ Form::open(array('route' => array('question.destroy', $question->id), 'method' => 'delete',
                                                    'class' => 'action-buttons')) }}
                                    {{ HTML::linkRoute('question.edit', trans('question-groups.edit'), ['id' => $question->id],
                                    ['class' => 'btn btn-xs btn-primary']) }}

                                    {{ Form::submit(trans('questions.delete_btn'), array('class'=>'btn btn-xs btn-warning'))}}
                                {{ Form::close() }}
                            </div>
                            <p class="col-md-12 question-description">{{ $question->correct_answers }}
                                {{ Lang::choice('question-groups.correct_answers', $question->correct_answers) }}</p>
                        </div>
                    </div>
                    <?php $i++; ?>
                    @endforeach
                </div>

                {{ Form::open(array('route'=>'question.store', 'class'=>'col-md-8 col-md-offset-2', 'id'=>'question-form')) }}

                {{ Form::hidden('group_id', $question_group->id) }}

                <h3>{{ trans('question-groups.add_question') }}</h3>

                @if ($errors->has())
                    @include('layouts._errors')
                @endif

                <p>{{ trans('question-groups.random_sort') }}</p>

                <div class="form-group">
                    {{ Form::text('question', null, array('class'=>'form-control', 'placeholder'=>trans('question-groups.question?'))) }}
                </div>

                <div class="form-group">
                    <button type="button" class="btn btn-large btn-primary add_answer">
                        {{ trans('question-groups.add_answer_btn') }}</button>
                </div>

                <div class="answer-area">
                    @if (Session::has('old_answers'))
                        <?php $i = 0; ?>
                        @foreach (Session::get('old_answers') as $answer)
                            @include('question-groups._answer')
                        <?php $i++; ?>
                        @endforeach
                    @else
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-1">
                                {{ Form::checkbox('answer[0][correct]', 1, true, ['title' => trans('question-groups.correct_ans')]) }}
                            </div>
                            <div class="col-md-10">
                                {{ Form::text('answer[0][answer_text]', null, array('class'=>'form-control',
                                'placeholder'=>trans('question-groups.answer'), 'type'=>'number')) }}
                            </div>
                            <div class="col-md-1">
                                <button type="button" class="btn btn-large btn-warning remove_answer"
                                        title="{{ trans('question-groups.remove') }}">X</button>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>

                <div class="form-group">
                    {{ Form::submit(trans('question-groups.save_and_add_btn'), array(
                                    'class'=>'btn btn-large btn-success btn-block')) }}
                </div>
                {{ Form::close() }}
            </div>