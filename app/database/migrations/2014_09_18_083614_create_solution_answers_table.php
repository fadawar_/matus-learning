<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolutionAnswersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('solution_answers', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('solution_question_id');
            $table->unsignedInteger('answer_id');
            $table->timestamps();

            $table->foreign('solution_question_id')
                ->references('id')->on('solution_questions');
            $table->foreign('answer_id')
                ->references('id')->on('answers');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('solution_answers');
    }

}
