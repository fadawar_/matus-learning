<ul>
    <?php
        $answers = $question->answers->toArray();
        shuffle($answers)
    ?>
    @foreach ($answers as $answer)
        <li>{{ Form::checkbox('questions['.$question->id.'][]', $answer['id']) }} {{ $answer['text'] }}</li>
    @endforeach
</ul>