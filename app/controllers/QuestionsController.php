<?php
/**
 * Created by PhpStorm.
 * User: fadawar
 * Date: 31.8.2014
 * Time: 22:47
 */

class QuestionsController extends BaseController {
    protected $layout = 'layouts.master';

    public function __construct()
    {
        $this->beforeFilter('csrf', array('on' => 'post'));
        $this->beforeFilter('auth');
        $this->beforeFilter('auth.admin');
    }

    public function show($id)
    {
        $question = Question::findOrFail($id);
        $answers = $question->answers->all();

        $this->layout->question = $question;
        $this->layout->answers = $answers;
        $this->layout->title = trans('questions.show_heading');
        $this->layout->content = 'questions.show';
    }

    public function edit($id)
    {
        $question = Question::findOrFail($id);
        $answers = $question->answers->all();

        $this->layout->question = $question;
        $this->layout->answers = $answers;
        $this->layout->title = trans('questions.edit_heading');
        $this->layout->content = 'questions.edit';
    }

    public function store()
    {
        Validator::extend('answers', 'QuestionsController@validate_answers');
        $validator = Validator::make(Input::all(), Question::$rules);

        if ($validator->passes()) {
            $this->saveQuestionsAndAnswersFromInput();

            return Redirect::to(URL::previous() . '#question-form')
                ->with('msg_type', 'success')
                ->with('message', trans('question-groups.question_saved'));
        } else {
            return Redirect::to(URL::previous() . '#question-form')
                ->with('msg_type', 'danger')
                ->with('message', trans('question-groups.err_occurred'))
                ->with('old_answers', Input::get('answer'))
                ->withErrors($validator)
                ->withInput();
        }
    }

    private function saveQuestionsAndAnswersFromInput()
    {
        $answers = array();
        $correct_answers = 0;
        foreach (Input::get('answer') as $answer) {
            $answers[] = new Answer(array(
                'correct'   => isset($answer['correct']),
                'text'      => $answer['answer_text'],
            ));
            if (isset($answer['correct'])) {
                $correct_answers++;
            }
        }

        $question = new Question(array(
            'correct_answers'   => $correct_answers,
            'text'              => Input::get('question'),
            'question_group_id' => Input::get('group_id'),
        ));
        $question->save();
        $question->answers()->saveMany($answers);
        return $question->id;
    }

    public function update($id)
    {
        Validator::extend('answers', 'QuestionsController@validate_answers');
        $validator = Validator::make(Input::all(), Question::$rules);

        if ($validator->passes()) {
            $question = Question::findOrFail($id);
            $question->delete();
            $new_question_id = $this->saveQuestionsAndAnswersFromInput();

            return Redirect::route('question.edit', [$new_question_id])
                ->with('msg_type', 'success')
                ->with('message', trans('questions.question_updated'));
        } else {
            return Redirect::to(URL::previous())
                ->with('msg_type', 'danger')
                ->with('message', trans('question-groups.err_occurred'))
                ->with('old_answers', Input::get('answer'))
                ->withErrors($validator)
                ->withInput();
        }
    }

    public function destroy($id)
    {
        $question = Question::findOrFail($id);
        $group_id = $question->questionGroup->id;
        $question->delete();

        return Redirect::route('question-groups.show', array('id' => $group_id))
            ->with('msg_type', 'success')
            ->with('message', trans('questions.destroy_succ'));
    }

    public function validate_answers($attribute, $value, $parameters)
    {
        $at_least_one_correct = false;
        foreach ($value as $answer) {
            if (isset($answer['correct'])) {
                $at_least_one_correct = true;
            }
            if (!isset($answer['answer_text']) || strlen($answer['answer_text']) == 0) {
                return false;
            }
        }
        return $at_least_one_correct;
    }
} 