            <div class="row">
                <h2>{{ $title }}</h2>

                {{ HTML::linkRoute('test.show', trans('users.start_test_btn'), null,
                                    ['class' => 'btn btn-success']) }}
                {{ HTML::linkRoute('test-solutions.show', trans('users.show_sol_btn'), ['id' => Auth::id()],
                                    ['class' => 'btn btn-primary']) }}
                {{ HTML::linkRoute('test-solutions.show', trans('users.req_new_test_btn'), ['id' => Auth::id()],
                                    ['class' => 'btn btn-info']) }}
            </div>