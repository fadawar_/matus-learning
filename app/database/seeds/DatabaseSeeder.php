<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		 $this->call('UsersTableSeeder');
		 $this->call('QuestionGroupsTableSeeder');
		 $this->call('QuestionsTableSeeder');
	}

}

class UsersTableSeeder extends Seeder {

    public function run()
    {
        $user = new User();
        $user->firstname = 'Jozef';
        $user->lastname = 'Gáborík';
        $user->email = 'jozef.gaborik@gmail.com';
        $user->password = Hash::make('infernal');
        $user->admin = true;
        $user->save();
    }

}

class QuestionGroupsTableSeeder extends Seeder {

    public function run()
    {
        $group = new QuestionGroup();
        $group->name = 'Ľahké';
        $group->points = 1;
        $group->save();

        $group = new QuestionGroup();
        $group->name = 'Ťažké';
        $group->points = 5;
        $group->save();
    }

}

class QuestionsTableSeeder extends Seeder {

    public function run()
    {
        // question 1
        $group = QuestionGroup::where('name', '=', 'Ľahké')->first();
        $answers[] = new Answer(array(
            'correct'   => true,
            'text'      => '9',
        ));

        $answers[] = new Answer(array(
            'correct'   => false,
            'text'      => '8',
        ));

        $answers[] = new Answer(array(
            'correct'   => true,
            'text'      => '10',
        ));

        $question = new Question(array(
            'correct_answers'   => 2,
            'text'              => "Koľko je hodín?",
            'question_group_id' => $group->id,
        ));
        $question->save();
        $question->answers()->saveMany($answers);


        // question 2
        $answers = array();
        $answers[] = new Answer(array(
            'correct'   => true,
            'text'      => 'sdf',
        ));

        $answers[] = new Answer(array(
            'correct'   => false,
            'text'      => 'sdfdfg',
        ));

        $question = new Question(array(
            'correct_answers'   => 1,
            'text'              => "sadg srfwe?",
            'question_group_id' => $group->id,
        ));
        $question->save();
        $question->answers()->saveMany($answers);



        //====================================================
        // other group
        // question 1
        $group = QuestionGroup::where('name', '=', 'Ťažké')->first();
        $answers = array();
        $answers[] = new Answer(array(
            'correct'   => true,
            'text'      => '7',
        ));

        $answers[] = new Answer(array(
            'correct'   => false,
            'text'      => '8',
        ));

        $question = new Question(array(
            'correct_answers'   => 1,
            'text'              => "Koľko je dni v tyzdni?",
            'question_group_id' => $group->id,
        ));
        $question->save();
        $question->answers()->saveMany($answers);
    }

}