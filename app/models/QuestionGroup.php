<?php

class QuestionGroup extends Eloquent {

    protected $table = 'question_groups';

    public static $rules = array(
        'name' => 'required|min:2',
        'points' => 'required|integer',
    );

    public function questions()
    {
        return $this->hasMany('Question');
    }
} 