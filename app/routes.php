<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::pattern('id', '[0-9]+');

Route::get('/', function()
{
	return View::make('layouts.master');
});

Route::controller('users', 'UsersController');
Route::resource('question-groups', 'QuestionGroupsController');
Route::resource('question', 'QuestionsController', array('only' => array('store', 'show', 'edit', 'update', 'destroy')));
Route::resource('test-solutions', 'TestSolutionsController', array('only' => array('store', 'show')));

Route::get('test/edit', array('as' => 'test.edit', 'before' => 'auth|auth.admin', 'uses' => 'TestController@edit'));
Route::get('test/show', array('as' => 'test.show', 'before' => 'auth|can_write_test', 'uses' => 'TestController@show'));
Route::put('test/update', array('as' => 'test.update', 'before' => 'auth|auth.admin', 'uses' => 'TestController@update'));