            <div class="row">
                {{ Form::open(array('route'=>'question-groups.store', 'class'=>'col-md-6 col-md-offset-3')) }}
                <h2 class="form-signup-heading">{{ $title }}</h2>

                @if ($errors->has())
                    @include('layouts._errors')
                @endif

                <div class="form-group">
                    {{ Form::text('name', null, array('class'=>'form-control', 'placeholder'=>trans('question-groups.name_field'))) }}
                </div>
                <div class="form-group">
                    {{ Form::text('points', null, array('class'=>'form-control',
                                  'placeholder'=>trans('question-groups.points_field'), 'type'=>'number')) }}
                </div>

                <div class="form-group">
                    {{ Form::submit(trans('question-groups.save_btn'), array('class'=>'btn btn-large btn-success btn-block'))}}
                </div>
                {{ Form::close() }}
            </div>
