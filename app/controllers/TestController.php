<?php
/**
 * Created by PhpStorm.
 * User: fadawar
 * Date: 8.9.2014
 * Time: 22:07
 */

class TestController extends BaseController {

    protected $layout = 'layouts.master';

    public function __construct()
    {
        $this->beforeFilter('csrf', array('on' => 'post'));
    }

    public function edit()
    {
        $question_groups = QuestionGroup::
            select(['question_groups.*', DB::raw('COUNT(questions.question_group_id) as questions_count')])
            ->join('questions', 'questions.question_group_id', '=', 'question_groups.id')
            ->whereNull('questions.deleted_at')
            ->groupBy('question_groups.id')
            ->orderBy('name')
            ->get();
        $this->layout->question_groups = $question_groups;
        $this->layout->title = trans('test.edit_title');
        $this->layout->content = 'test.edit';
    }

    public function update()
    {
        Validator::extend('questions_count_control', 'TestController@validate_questions_count');
        $validator = Validator::make(Input::all(), array('questions_count' => 'required|array|questions_count_control'));

        if ($validator->passes()) {
            foreach (Input::get('questions_count') as $id => $count) {
                $group = QuestionGroup::find($id);
                if ($group->questions_in_test != $count) {
                    $group->questions_in_test = $count;
                    $group->save();
                }
            }

            return Redirect::route('test.edit')
                ->with('msg_type', 'success')
                ->with('message', trans('test.success'));
        }
        else {
            return Redirect::route('test.edit')
                ->with('msg_type', 'danger')
                ->with('message', trans('test.solution_id_not_found'))
                ->withErrors($validator)
                ->withInput();
        }
    }

    public function show()
    {
        $generated_test = $this->generateTest();
        $user = Auth::user();
        if (!$user->isAdmin()) {
            Auth::user()->can_write_test = false;
            Auth::user()->save();
        }

        $this->layout->question_areas = $generated_test['question_areas'];
        $this->layout->test_solution_id = $generated_test['test_solution_id'];
        $this->layout->title = trans('test.show_title');
        $this->layout->content = 'test.show';
    }

    public function validate_questions_count($attribute, $value, $parameters)
    {
        foreach ($value as $id => $count) {
            $group = QuestionGroup::find($id);
            $max_count = $group->questions()->count();
            if ($count < 0 || $count > $max_count) {
                return false;
            }
        }
        return true;
    }

    private function generateTest()
    {
        $max_points = 0;
        $question_areas = array();
        $solution_questions = array();
        $groups = QuestionGroup::where('questions_in_test', '>', '0')->get();
        foreach ($groups as $group) {
            $max_points += $group->points * $group->questions_in_test;
            $area = $group->questions()->orderByRaw('random()')->limit($group->questions_in_test)
                ->with('answers')->get();
            $question_areas[] = $area;

            foreach($area as $question) {
                $solution_questions[] = new SolutionQuestion(array(
                    'question_id'       => $question->id));
            }
        }

        $test_solution = new TestSolution(array(
            'user_id'       => Auth::id(),
            'max_points'    => $max_points));

        $test_solution->save();
        $test_solution->solutionQuestions()->saveMany($solution_questions);

        $results['question_areas'] = $question_areas;
        $results['test_solution_id'] = $test_solution->id;

        return $results;
    }
} 