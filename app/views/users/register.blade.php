            <div class="row">
                {{ Form::open(array('url'=>'users/create', 'class'=>'col-md-6 col-md-offset-3')) }}
                <h2 class="form-signup-heading">{{ trans('users.reg_heading') }}</h2>

                @if ($errors->has())
                    @include('layouts._errors')
                @endif

                <div class="form-group">
                    {{ Form::text('firstname', null, array('class'=>'form-control', 'placeholder'=>trans('users.firstname_field'))) }}
                </div>
                <div class="form-group">
                    {{ Form::text('lastname', null, array('class'=>'form-control', 'placeholder'=>trans('users.lastname_field'))) }}
                </div>
                <div class="form-group">
                    {{ Form::text('email', null, array('class'=>'form-control', 'placeholder'=>trans('users.email_field'))) }}
                </div>
                <div class="form-group">
                    {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>trans('users.password_field'))) }}
                </div>
                <div class="form-group">
                    {{ Form::password('password_confirmation', array('class'=>'form-control', 'placeholder'=>trans('users.pass_conf_field'))) }}
                </div>

                <div class="form-group">
                    {{ Form::submit(trans('users.reg_btn'), array('class'=>'btn btn-large btn-primary btn-block'))}}
                </div>
                {{ Form::close() }}
            </div>