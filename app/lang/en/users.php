<?php

return array(
    /* Login page */
    'login_heading'     => 'Please Login',
    'email_field'       => 'Email Address',
    'password_field'    => 'Password',
    'login_btn'         => 'Login',

    /* Register page */
    'reg_title'         => 'Registration',
    'reg_heading'       => 'Please Register',
    'firstname_field'   => 'First Name',
    'lastname_field'    => 'Last Name',
    'pass_conf_field'   => 'Confirm Password',
    'reg_btn'           => 'Register',

    /* Dashboard page */
    'dash_title'        => 'Dashboard',
    'start_test_btn'    => 'Start test',
    'show_sol_btn'      => 'Show solution',
    'req_new_test_btn'  => 'Request new test',
);
