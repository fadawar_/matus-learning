
<div class="row">
    {{ Form::open(array('route'=>array('question.update', $question->id), 'method' => 'put',
                        'class'=>'col-md-8 col-md-offset-2', 'id'=>'question-form')) }}

    {{ Form::hidden('group_id', $question->question_group_id) }}

    <h3>{{ trans('questions.edit_heading') }}</h3>

    @if ($errors->has())
        @include('layouts._errors')
    @endif

    <div class="form-group">
        {{ Form::text('question', $question->text, array('class'=>'form-control')) }}
    </div>

    <div class="form-group">
        <button type="button" class="btn btn-large btn-primary add_answer">
            {{ trans('question-groups.add_answer_btn') }}</button>
    </div>

    <div class="answer-area">
        <?php $i = 0; ?>
        @foreach ($answers as $answer)
            @include('questions._answer')
            <?php $i++; ?>
        @endforeach
    </div>

    <div class="form-group">
        {{ Form::submit(trans('questions.update_btn'), array(
                        'class'=>'btn btn-large btn-success btn-block')) }}
    </div>
    {{ Form::close() }}
</div>