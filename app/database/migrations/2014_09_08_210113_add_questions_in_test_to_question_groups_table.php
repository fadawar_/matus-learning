<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuestionsInTestToQuestionGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('question_groups', function(Blueprint $table) {
            $table->unsignedInteger('questions_in_test')->default(0);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('question_groups', function(Blueprint $table) {
            $table->dropColumn('questions_in_test');
        });
	}

}
