<?php

class Question extends Eloquent {
    use SoftDeletingTrait;

    protected $dates = ['deleted_at'];
    protected $fillable = array('correct_answers', 'text', 'question_group_id');

    public static $rules = array(
        'question'      => 'required|min:2',
        'answer'        => 'required|array|min:2|answers',
        'group_id'      => 'required|integer',
    );

    public function questionGroup()
    {
        return $this->belongsTo('QuestionGroup');
    }

    public function answers()
    {
        return $this->hasMany('Answer');
    }
} 