        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">trans('navigation.toggle_navigation')</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    {{ HTML::link('/', trans('navigation.app_name'), array('class' => 'navbar-brand')) }}
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        @if (Auth::check())
                        <li>{{ HTML::link('users/dashboard', trans('navigation.dashboard')) }}</li>
                            @if (Auth::user()->isAdmin())
                            <li>{{ HTML::linkRoute('question-groups.index', trans('navigation.question_groups')) }}</li>
                            <li>{{ HTML::linkRoute('test.edit', trans('navigation.test')) }}</li>
                            @endif
                        <li>{{ HTML::link('users/logout', trans('navigation.logout')) }}</li>
                        @else
                            <li>{{ HTML::link('users/register', trans('navigation.register')) }}</li>
                            <li>{{ HTML::link('users/login', trans('navigation.login')) }}</li>
                        @endif
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>