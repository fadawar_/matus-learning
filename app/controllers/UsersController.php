<?php

class UsersController extends BaseController
{
    protected $layout = 'layouts.master';

    public function __construct()
    {
        $this->beforeFilter('csrf', array('on' => 'post'));
        $this->beforeFilter('auth', array('only' => array('getDashboard')));
    }

    public function getRegister()
    {
        $this->layout->title = trans('users.reg_title');
        $this->layout->content = 'users.register';
    }

    public function postCreate()
    {
        $validator = Validator::make(Input::all(), User::$rules);

        if ($validator->passes()) {
            $user = new User;
            $user->firstname = Input::get('firstname');
            $user->lastname = Input::get('lastname');
            $user->email = Input::get('email');
            $user->password = Hash::make(Input::get('password'));
            $user->save();

            return Redirect::to('users/login')
                ->with('msg_type', 'success')->with('message', 'Thanks for registering!');
        } else {
            return Redirect::to('users/register')
                ->with('message', 'The following errors occurred')->withErrors($validator)->withInput();
        }
    }

    public function getLogin()
    {
        $this->layout->content = 'users.login';
    }

    public function postSignin()
    {
        if (Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password')))) {
            return Redirect::to('users/dashboard')
                ->with('msg_type', 'success')
                ->with('message', 'You are now logged in!');
        } else {
            return Redirect::to('users/login')
                ->with('msg_type', 'warning')
                ->with('message', 'Your username/password combination was incorrect')
                ->withInput();
        }
    }

    public function getDashboard()
    {
        $this->layout->title = trans('users.dash_title');
        $this->layout->content = 'users.dashboard';
    }

    public function getLogout()
    {
        Auth::logout();
        return Redirect::to('users/login')
            ->with('msg_type', 'info')
            ->with('message', 'Your are now logged out!');
    }
} 