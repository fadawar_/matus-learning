<?php

return array(
    'app_name'          => 'Matúš learning',
    'toggle_navigation' => 'Toggle navigation',

    'register'          => 'Register',
    'login'             => 'Login',
    'logout'            => 'Logout',
    'dashboard'         => 'Dashboard',
    'question_groups'   => 'Question groups',
    'test'              => 'Edit test',

);
